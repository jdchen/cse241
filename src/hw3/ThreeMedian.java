package hw3;

import java.util.Arrays;
import graph.*;
import java.awt.Color;
import java.lang.Math;

public class ThreeMedian {
    
    public static int i;
    public static int j;
	public static long[] durationList;
	public static int sL;
	public static int sR;
    
        
    public static boolean isCorrect(WeightedObject[] S, WeightedObject guess) {
		int leftSum=0;
		int rightSum=0;
		boolean atRight=false;
		for (int i=0; i<S.length-1;i++) {
			if (S[i]==guess) atRight=true;
			else if (!atRight) leftSum+=S[i].w;
			else rightSum+=S[i].w;
		}
		int totalSum=leftSum+rightSum+guess.w;
		if (leftSum<=totalSum/3&&rightSum<=totalSum*2/3) return true;
		else return false;
    }
	
	public static WeightedObject threeMedian(WeightedObject[] A){
		int totalSum=0;
		for (WeightedObject a:A) totalSum+=a.w;
		return findThreeMedian(A,0,A.length-1,totalSum/3,totalSum-totalSum/3);
		// sL=totalSum/3;
		// sR=totalSum-totalSum/3;
		// return findThreeMedian(A,0,A.length-1,0,0);
	}
	
	public static WeightedObject findThreeMedian(WeightedObject[] A,int p, int r,int sl, int sr) {
		int q = partition(A,p,r);
		int leftSum=0;
		int rightSum=0;
		for (int i=p;i<q;i++) leftSum+=A[i].w;
		for (int i=q+1;i<=r;i++) rightSum+=A[i].w;
		if (leftSum>sl) return findThreeMedian(A,p,q-1,sl,sr-A[q].w-rightSum);
		else if (rightSum>sr) return findThreeMedian(A,q+1,r,sl-A[q].w-leftSum,sr);
		else return A[q];
		// for (int i=0;i<q;i++) leftSum+=A[i].w;
		// for (int i=q+1;i<A.length;i++) rightSum+=A[i].w;
		// if (leftSum>sL) return findThreeMedian(A,p,q-1,0,0);
		// else if (rightSum>sR) return findThreeMedian(A,q+1,r,0,0);
		// else return A[q];
	}
	
	public static int partition(WeightedObject[] A,int p, int r) {
		i=p-1;
		j=p-1;
		WeightedObject temp;
		while (true) {
			j++;
			if (A[j].comp(A[r])) {
				i++;
				temp=A[i];
				A[i]=A[j];
				A[j]=temp;
			}
			if (j>=r-1) break;
		}
		temp=A[i+1];
		A[i+1]=A[r];
		A[r]=temp; 
		return i+1;
	}
    
	public static double[] toDouble(long[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
	
    public static void main(String[] args) {
		double[] nList = new double[100];
		long[] durationList = new long[100];
		for (int i=1; i<20; i++) {
			int n=(int)Math.pow(2,i);
			nList[i]=n; 
			WeightedObject[] a = Generator.getWeightedObjects(n);
			long startTime = System.currentTimeMillis();
			WeightedObject ans = threeMedian(a);
			long endTime = System.currentTimeMillis();
			durationList[i]=endTime-startTime;
			System.out.println("Size "+n+" data took "+durationList[i]+" to run");
			if (!isCorrect(a,ans)) {
				System.out.println("incorrect");
			}
		}
		Plot.plot(nList,toDouble(durationList),Color.GREEN,"Runtime analysis",true,false);
    }
}