package hw3;

import java.util.Arrays;
import java.util.LinkedList;
import graph.*;
import java.awt.Color;
import java.lang.Math;

public class Plurality {
    
    public static int i;
    public static int j;
	public static long[] durationList;
    
        
    public static boolean isCorrect(int[] A,int k, LinkedList<Integer> guess) {
		for (int g:guess) {
			int count=0;
			for (int i=0; i<A.length; i++) {
				if (A[i]==g) count++;
			}
			if (count<=A.length/k) return false;
		}
		return true;
    }
	
	public static LinkedList<Integer> findPlural(int[] A,int k){
		LinkedList<Integer> ans = new LinkedList<Integer>();
		for (int i=1;i<k;i++) {
			int selected=OSSelect(A,0,A.length-1,i*A.length/k);
			//System.out.println("selected:"+selected);
			int count=0;
			for (int j=0;j<A.length;j++) {
				if (A[j]==selected) count++;
			}
			if (count>A.length/k) ans.add(selected);
		}
		// System.out.print("ans:");
		// for (int i:ans) System.out.print(i+",");
		// System.out.println();
		return ans;
	}
	
	public static int OSSelect(int[] A, int p, int r, int i) {
		if (p==r) return A[p];
		int q=partition(A,p,r);
		int k=q-p;
		if (i==k) return A[q];
		else if (i<k) return OSSelect(A,p,q-1,i);
		else return OSSelect(A,q+1,r,i-k-1);
	}
	
	public static int partition(int[] A,int p, int r) {
		i=p-1;
		j=p-1;
		int temp;
		while (true) {
			j++;
			if (A[j]<A[r]) {
				i++;
				temp=A[i];
				A[i]=A[j];
				A[j]=temp;
			}
			if (j>=r-1) break;
		}
		temp=A[i+1];
		A[i+1]=A[r];
		A[r]=temp; 
		return i+1;
	}
    
	public static double[] toDouble(long[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
	
    public static void main(String[] args) {
		double[] nList = new double[100];
		long[] durationList = new long[100];
		int k=5;
		for (int i=1; i<8; i++) {
			int n=(int)Math.pow(10,i);
			nList[i]=n; 
			int[] a = Generator.getRepeatedElements(n,k);
			//int[] a = {1,2,3,4,1};
			long startTime = System.currentTimeMillis();
			LinkedList<Integer> guess = findPlural(a,k);
			long endTime = System.currentTimeMillis();
			durationList[i]=endTime-startTime;
			System.out.println("Size "+n+" data took "+durationList[i]+" to run");
			if (!isCorrect(a,k,guess)) {
				System.out.println("incorrect");
			}
		}
		Plot.plot(nList,toDouble(durationList),Color.GREEN,"Runtime analysis",true,false);
    }
}