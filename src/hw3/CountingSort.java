package hw3;

import java.util.Arrays;
import graph.*;
import java.awt.Color;
import java.lang.Math;

public class CountingSort {
    
    public static int i;
    public static int j;
	public static long[] durationList;
    
        
    public static boolean isSorted(int[] S) {
		for (int i=0; i<S.length-1;i++) {
			if (S[i+1]<S[i]) return false;
		}
		return true;
    }
	
	public static void countSort(int[] A){
		int[] C = new int[65537];
		int[] B = new int[A.length];
		for (int i=0; i<A.length; i++) {
			C[A[i]]++;
		}
		for (int i=1;i<65537;i++) {
			C[i]=C[i-1]+C[i];
		}
		int r = A.length-1;
		while (r>=0) {
			int a = A[r];
			int j = C[a]-1;
//			B[j]=a;
//			C[a]--;
			if (j>=r) {
				C[a]--;			
				while (j!=r) {
					System.out.println(j);
					int b = A[j];
					int k = C[b];
					A[j]=a;
					a=b; j=k-1;
					C[b]--;
				}
				A[j]=a;
			}
			r--;
		}
	}
    
	public static double[] toDouble(long[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
	
    public static void main(String[] args) {
		double[] nList = new double[100];
		long[] durationList = new long[100];
		for (int i=1; i<9; i++) {
			//int n=(int)Math.pow(10,i);
			int n=i*2;
			nList[i]=n; 
			int[] a = Generator.getRandomArray(n);
			//int[] a = {4,1,3,4,3,3,1,4};
			long startTime = System.currentTimeMillis();
			countSort(a);
			long endTime = System.currentTimeMillis();
			durationList[i]=endTime-startTime;
			System.out.println("Size "+n+" data took "+durationList[i]+" to run");
			if (!isSorted(a)) {
				System.out.println("incorrect");
			}
		}
		Plot.plot(nList,toDouble(durationList),Color.GREEN,"Runtime analysis",true,false);
    }
}