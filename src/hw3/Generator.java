package hw3;

import java.util.Random;
import java.awt.Color;
import java.util.LinkedList;
import java.util.Collections;
import graph.*;

public class Generator {
	
	public static Random random = new Random();
	public static LinkedList<Integer> temperatures; 
	
	public static int[] getRandomArray(int n) {
		int[] res = new int[n];
		for (int i=0; i<n; i++) {
			res[i]=random.nextInt(65536)+1;
		}
		return res;
	}
	
	public static int[] getRepeatedElements(int n,int k) {
		int[] res = new int[n];
		for (int i=0; i<n; i++) {
			res[i]=random.nextInt(k+1);
		}
		return res;
	}
	
	public static WeightedObject[] getWeightedObjects(int n) {
		WeightedObject[] res = new WeightedObject[n];
		for (int i=0; i<n; i++) {
			res[i]=new WeightedObject(random.nextDouble(),random.nextInt(10));
		}
		return res;
	
	}
	
	

}