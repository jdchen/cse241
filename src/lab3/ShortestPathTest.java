package lab3;

class ShortestPathTest {

    public static void main(String args[]) {
		System.out.println("Testing shortest path...");
		Multigraph G = new Multigraph();
		Vertex[] vertices = new Vertex[4];
		Edge[] edges = new Edge[5];
		for (int j = 0; j < vertices.length; j++) {
            vertices[j] = new Vertex(j);
            G.addVertex(vertices[j]);
        }
		edges[0]=new Edge(0,vertices[0],vertices[1],1);
		edges[1]=new Edge(1,vertices[0],vertices[2],1);
		edges[2]=new Edge(2,vertices[0],vertices[3],4);
		edges[3]=new Edge(3,vertices[1],vertices[3],2);
		edges[4]=new Edge(4,vertices[2],vertices[3],4);
		for (int j=0;j<edges.length;j++) {
			edges[j].from().addEdge(edges[j]);
		}
		
		for (int i=0;i<vertices.length;i++) {
			ShortestPaths sp = new ShortestPaths(G,i);
			for (int j=0;j<vertices.length;j++) {
				int[] path = sp.returnPath(j);
				String s="";
				for (int k:path) s+=k+"->";
				System.out.println("SP from "+i+" to "+j+" = "+s);
			}
		}
				
		
	}
	
}