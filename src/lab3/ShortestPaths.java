package lab3;

import java.util.ArrayList;

/**
 * Compute shortest paths in a graph.
 *
 * Your constructor should compute the actual shortest paths and
 * maintain all the information needed to reconstruct them.  The
 * returnPath() function should use this information to return the
 * appropriate path of edge ID's from the start to the given end.
 *
 * Note that the start and end ID's should be mapped to vertices using
 * the graph's get() function.
 */
class ShortestPaths {

		ArrayList<Integer>[] path;
    
    /**
     * Constructor
     */
    public ShortestPaths(Multigraph G, int startId) {
		PriorityQueue<Vertex> Q = new PriorityQueue();
		int n = G.nVertices();
		int[] d = new int[n];
		path = new ArrayList[n];
		for (int i=0;i<n;i++) {
			Vertex v = G.get(i);
			if (i==startId) {Q.insert(0,v); d[i]=0;}
			else {Q.insert(Integer.MAX_VALUE,v); d[i]=Integer.MAX_VALUE;}
			path[i]=new ArrayList<Integer>();
		}
		while (!Q.isEmpty()) {
			Vertex u = Q.extractMin();
			int uDistance = d[u.id()];
			if (uDistance==Integer.MAX_VALUE) continue;
			Vertex.EdgeIterator ite = u.adj();
			while (ite.hasNext()) {
				Edge e = ite.next();
				Vertex v = e.to();
				int vDistance = d[v.id()];
				int newDistance = uDistance + e.weight();
				if (vDistance>newDistance) {
					d[v.id()]=newDistance;
					path[v.id()]=new ArrayList<Integer>(path[u.id()]);
					path[v.id()].add(e.id());
				}
			}
		}
    }
    
    /**
     * Calculates the list of edge ID's forming a shortest path from the start
     * vertex to the specified end vertex.
     *
     * @return the array
     */
    public int[] returnPath(int endId) { 
       // YOUR CODE HERE
	   if (path[endId].size()==0) return new int[0];
	   int[] result = new int[path[endId].size()];
	   for (int i=0;i<result.length;i++) {
			result[i]=path[endId].get(i);
	   }	
       return result;
    }
}
