package lab3;

class PQNode<T> {

	int key;
	T value;
	Handle handle;

	public PQNode(int key, T value) {
		this.key=key;
		this.value=value;
	}
}
