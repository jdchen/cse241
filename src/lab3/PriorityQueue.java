package lab3;

import java.util.ArrayList;

/**
 * A priority queue class supporting operations needed for
 * Dijkstra's algorithm.
 */
class PriorityQueue<T> {

	ArrayList<PQNode<T>> list;

    /**
     * Constructor
     */
    public PriorityQueue() {
		list = new ArrayList<PQNode<T>>(20);
    }

    /**
     * @return true iff the queue is empty.
     */
    public boolean isEmpty() {
       return list.isEmpty();
    }
	
	/**
     * swaps the nodes at index a and b
     */
    public void swap(int a, int b) {
       if (a<0||a>=list.size()||b<0||b>=list.size()) return;
	   PQNode<T> temp = list.get(a);
	   list.set(a,list.get(b));
	   list.set(b,temp);
	   list.get(a).handle.index=a;
	   list.get(b).handle.index=b;
    }
	
	/**
     * fixes the heap if the heap property holds everywhere except 
	 * at index i
     */
    public void heapify(int i) {
		int endOfArray=list.size()-1;
		int left = i*2+1,right = i*2+2;
		int y;
		if (left>endOfArray&&right>endOfArray) return;
		else if (left>endOfArray) y=right;
		else if (right>endOfArray) y=left;
		else if (list.get(left).key<list.get(right).key) y=left;
		else y=right;
		if (list.get(y).key<list.get(i).key) {
			swap(i,y);
			heapify(y);
		}
	}

    /**
     * Insert a (key, value) pair into the queue.
     *
     * @return a Handle to this pair so that we can find it later.
     */
    Handle insert(int key, T value) {
		PQNode<T> node = new PQNode<T>(key,value);
		list.add(node);
		int i=list.size()-1;
		node.handle = new Handle(i);
		while(i>0&&list.get(i).key<list.get((i-1)/2).key) {
			swap(i,(i-1)/2);
			i=(i-1)/2;
		}
		return node.handle;
    }

    /**
     * @return the min key in the queue.
     */
    public int min() {
       return list.get(0).key;
    }

    /**
     * Find and remove the smallest (key, value) pair in the queue.
     *
     * @return the value associated with the smallest key
     */
    public T extractMin() {
	   if (isEmpty()) return null;
	   PQNode<T> first = list.get(0);
       T result=first.value;
	   swap(0,list.size()-1);
	   list.remove(list.size()-1);
	   heapify(0);
	   first.handle.index=-1; //invalidate handle of removed node
	   return result;
    }


    /**
     * Decrease the key to the newKey associated with the Handle.
     *
     * If the pair is no longer in the queue, or if its key <= newKey,
     * do nothing and return false.  Otherwise, replace the key with newkey,
     * fix the ordering of the queue, and return true.
     *
     * @return true if we decreased the key, false otherwise.
     */
    public boolean decreaseKey(Handle h, int newKey) {
		if (h.index==-1) return false;
		if (newKey>=handleGetKey(h)) return false;
		int i=h.index;
		list.get(i).key=newKey;
		while(i>0&&list.get(i).key<list.get((i-1)/2).key) {
			swap(i,(i-1)/2);
			i=(i-1)/2;
		}
		return true;
    }

    /**
     * @return the key associated with the Handle.
     */
    public int handleGetKey(Handle h) {
		if (h.index==-1) return -1;
       return list.get(h.index).key;
    }

    /**
     * @return the value associated with the Handle.
     */
    public T handleGetValue(Handle h) {
		if (h.index==-1) return null;
       return list.get(h.index).value;
    }

    /**
     * Print every element of the queue in the order in which it appears
     * (i.e. the array representing the heap)
     */
    public String toString() {
		String s ="";
		for (PQNode<T> n: list) {
			s+="("+n.key+" "+n.value+"),";
		}
       return s;
    }
}
