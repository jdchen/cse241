package hw1;

import java.awt.Color;
import graph.*;
import java.util.Collections;
import java.util.LinkedList;

public class PolygonSearch {

	public static int[] yValues;
	public static int currentL;
	public static int currentR;
	public static int mid;
	public static int numIterations;

	public static int findMaxElementX(int[] x) {
		yValues=x;
		currentL=0;
		currentR=x.length-1;
		numIterations=0;
		while(currentL!=currentR) {
			numIterations++;
			mid = (currentR+currentL)/2; //floor
			if (yValues[mid+1]>yValues[mid]) currentL=mid+1;
			else currentR=mid;
		}
		return yValues[currentL];
	}
	
	public static int findMaxElementY(int[] y) {
		yValues=y;
		currentL=0;
		currentR=y.length-1;
		numIterations=0;
		while(currentL!=currentR) {
			numIterations++;
			mid = (currentR+currentL)/2; //floor
			if (yValues[currentL+1]>yValues[currentL]) {
				//System.out.println("1");
				if (yValues[mid+1]>yValues[mid]) currentL=mid+1;
				else currentR=mid;
			} else if (yValues[currentR]<yValues[currentR-1]) {
				//System.out.println("2");
				if (yValues[mid+1]>yValues[mid]||yValues[mid]<yValues[currentR]) 
					currentL=mid+1;
				else currentR=mid;
			} else {
				//System.out.println("3");
				if (yValues[currentL]>yValues[currentR])
					currentR=currentL;
				else currentL=currentR;
			}
		}
		//check correctness
		LinkedList<Integer> list = new LinkedList<Integer>();
		for (int i:yValues) {list.add(new Integer(i)); 
			//System.out.print("'"+i+"'");
		}
		if (yValues[currentL]==Collections.max(list).intValue()) System.out.println(true);
		else System.out.println(false);
		return yValues[currentL];
	}
	
	public static void main(String[] args) {
		double[] nList = new double[100];
		double[] durationList = new double[100];
		double[] iterationList = new double[100];
		for (int i=0; i<100; i++) {
			int[][] a = Generator.getPolygon();
			// int[][] a = new int[2][7];
			// int[] a0={0,1,2,3,4,1,0};
			// int[] a1 ={0,-4,-7,-9,-10,-11,0};
			// a[0]=a0; a[1]=a1;
			int n = a[0].length;
			//System.out.print("n="+n+"   ");
			long startTime = System.currentTimeMillis();
			int max = findMaxElementY(a[1]);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			//System.out.print(duration+"ms    ");
			//System.out.println(numIterations+" iterations");
			double[] b ={a[0][currentL%n]};
			double[] c ={max};
			//Plot.plot(b,c,Color.RED,"PolygonSearch",true,false);
			//Plot.plot(Generator.toDouble(a[0]),Generator.toDouble(a[1]),Color.YELLOW,"test",false,false);
			nList[i]=n; 
			durationList[i]=(double)duration;
			iterationList[i]=numIterations;
		}
		//Plot.plot(nList,iterationList,Color.GREEN,"Runtime analysis",true,false);
	}
	
	
	
}