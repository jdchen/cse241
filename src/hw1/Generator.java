package hw1;

import java.util.Random;
import java.awt.Color;
import graph.*;

public class Generator {
	
	public static Random random = new Random();
	
	public static int[][] getUnimodal() {
		int n = (int)(Math.exp(random.nextInt(15))+1);
		int m = random.nextInt(n)+1;
		int[] x = new int[n];
		int[] y = new int[n];
		int current = random.nextInt(100)+1;
		for (int i=0; i<n; i++) {
			x[i]=i; 
			if (i<=m) current+=random.nextInt(100)+1;
			else current-=random.nextInt(100)+1;
			y[i]=current;
		}
		int[][] result = new int[2][n];
		result[0]=x;
		result[1]=y;
		return result;
	}
	
	public static int[][] getPolygon() {
		int n = random.nextInt(10)+3;
		//boolean isUpwards = random.nextInt(2)==0;
		boolean isUpwards = false; //always counterclockwise
		int[] vx = new int[n];
		int[] vy = new int[n];
		double[] angle = new double[n];
		int[] x = new int[n];
		int[] y = new int[n];
		int[] sortedVX = new int[n];
		int[] sortedVY = new int[n];
		int sumX = 0;
		int sumY = 0;
		for (int i=0; i<n-2; i++) {
			vx[i] = random.nextInt(100)+1;
			vy[i] = random.nextInt(100)+1;
			if (random.nextInt(2)==0) vx[i]=-vx[i];
			if (random.nextInt(2)==0) vy[i]=-vy[i];
			sumX+=vx[i]; sumY+=vy[i];
			angle[i]=Math.atan2(vy[i],vx[i]);
		}
		if (sumX==0) {
			vx[n-2]=random.nextInt(10)+1;
			vx[n-1]=-vx[n-2];
		} else {
			vx[n-2]=-1;
			vx[n-1]=-sumX+1;
		}
		if (sumY==0) {
			vy[n-2]=random.nextInt(10)+1;
			vy[n-1]=-vy[n-2];
		} else {
			vy[n-2]=-1;
			vy[n-1]=-sumY+1;
		}
		angle[n-2]=Math.atan2(vy[n-2],vx[n-2]);
		angle[n-1]=Math.atan2(vy[n-1],vx[n-1]);
		x[0]=random.nextInt(10)+1;
		y[0]=random.nextInt(10)+1;
		for (int i=0; i<n; i++) {
			if (isUpwards) angle[i]=-angle[i];
			if (angle[i]<-Math.PI/2) angle[i]+=Math.PI*2;
		}
		for (int i=0; i<n-1; i++) {
			double minAngle=Double.POSITIVE_INFINITY;
			int minIndex=0;
			for (int j=0;j<n;j++) {
				if (angle[j]<minAngle) {
					sortedVX[i]=vx[j];
					sortedVY[i]=vy[j];
					minAngle=angle[j];
					minIndex=j;
				}
			}
			angle[minIndex]=Double.POSITIVE_INFINITY;
			x[i+1]=x[i]+sortedVX[i];
			y[i+1]=y[i]+sortedVY[i];
		}
		int[][] result = new int[2][n];
		result[0]=x;
		result[1]=y;
		return result;
	}
	
	public static int[][] getPolygonTest() {
		int[][] result = new int[2][4];
		int[] a={0,1,2,3};
		int[] b={0,-10,-8,-5};
		result[0]=a;
		result[1]=b;
		return result;
	}
	
	public static double[] toDouble(int[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
	
	public static void main(String[] args) {
		int[][] a = Generator.getUnimodal();
		Plot.plot(toDouble(a[0]),toDouble(a[1]),Color.RED,"first plot",false,false);
		a = Generator.getUnimodal();
		Plot.plot(toDouble(a[0]),toDouble(a[1]),Color.GREEN,"second plot",true,true);
		a = Generator.getUnimodal();
		Plot.plot(toDouble(a[0]),toDouble(a[1]),Color.YELLOW,"doesnt matter",false,false);
	}
	
	

}