package hw1;

import java.awt.Color;
import graph.*;

public class UnimodalSearch {

	public static int[] yValues;
	public static int currentL;
	public static int currentR;
	public static int mid;
	public static int numIterations;

	public static int findMaxElement(int[] y) {
		yValues=y;
		currentL=0;
		currentR=y.length-1;
		numIterations=0;
		while(currentL!=currentR) {
			numIterations++;
			mid = (currentR+currentL)/2; //floor
			if (yValues[mid+1]>yValues[mid]) currentL=mid+1;
			else currentR=mid;
		}
		return yValues[currentL];
	}	
	
	public static void main(String[] args) {
		double[] nList = new double[20];
		double[] durationList = new double[20];
		double[] iterationList = new double[20];
		for (int i=0; i<20; i++) {
			int[][] a = Generator.getUnimodal();
			int n = a[0].length;
			System.out.print("n="+n+"   ");
			long startTime = System.currentTimeMillis();
			int max = findMaxElement(a[1]);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			System.out.print(duration+"ms    ");
			System.out.println(numIterations+" iterations");
			//double[] b ={currentL};
			//double[] c ={max};
			//Plot.plot(b,c,Color.RED,"Unimodal Search",true,false);
			//Plot.plot(Generator.toDouble(a[0]),Generator.toDouble(a[1]),Color.YELLOW,"test",false,false);
			nList[i]=n; 
			durationList[i]=(double)duration;
			iterationList[i]=numIterations;
		}
		Plot.plot(nList,iterationList,Color.GREEN,"Runtime analysis",true,false);
	}
	
	
	
}