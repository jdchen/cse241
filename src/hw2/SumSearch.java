package hw2;

import java.util.Arrays;
import graph.*;
import java.awt.Color;
import java.lang.Math;

public class SumSearch {
    
    public static int i;
    public static int j;
	public static long[] durationList;
    
        
    public static boolean findSum(int[] S, int z) {
        Arrays.sort(S);
        int left, right, mid, target;
        //for (i=0; i<S.length; i++) {
        //    System.out.println(S[i]);
        //}
        for (i=0; i<S.length-1; i++){
            target=z-S[i];
            left=i+1;
            right=S.length-1;
            while  (true) {
                mid=(left+right)/2;
                if (target==S[mid]) {
                    j=mid; return true;
                } else if (target>S[mid]&&mid!=right) {
                    left=mid+1;
                } else if (target<S[mid]&&mid!=left) {
                    right=mid-1;
                } else break;
            }
        }
        return false;
    }
	
	public static boolean countSum(int[] S, int z) {
		int n=S.length;
		int[] count = new int[65537]; 
		for (int i=0;i<n; i++) {
			count[S[i]]++;
		}
		for (int j=1; j<=65536;j++) {
			if (count[j]>0&&z-j>0&&z-j<65537&&count[z-j]>0)
				return true;
		}
		return false;
	}
	
	public static double[] toDouble(long[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
    
    public static void main(String[] args) {
		double[] nList = new double[100];
		long[] durationList = new long[100];
		for (int i=1; i<9; i++) {
			int n=(int)Math.pow(10,i);
			nList[i]=n; 
			int[] a = Generator.getRandomArray(n);
			int b = Generator.getSum();
			System.out.println("z = "+b);
			boolean test=findSum(a,b);
			long startTime = System.currentTimeMillis();
			boolean result=countSum(a,b);
			long endTime = System.currentTimeMillis();
			durationList[i]=endTime-startTime;
			System.out.println("Size "+n+" data took "+durationList[i]+" to run");
			if (test&&!result||!test&&result) System.out.println("incorrect");
			else System.out.println("correct");
		}
		Plot.plot(nList,toDouble(durationList),Color.GREEN,"Runtime analysis",true,false);
    }
}