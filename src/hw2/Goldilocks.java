package hw2;

import graph.*;
import java.awt.Color;
import java.lang.Math;
import java.util.Random;

public class Goldilocks {
    
    public int preference;
	public static Goldilocks[] G;
	public static Porridge[] P;
	public static int numIterations;
	public static Random random=new Random();
    
    public Goldilocks(int preference) {
        this.preference = preference;
    }
    
    public String testPorridge(Porridge p) {
		numIterations++;
        if (p.getTemperature()==preference) return "Yummm";
        else {
            //return "Yuck!"
            if (p.getTemperature()>preference)
                return "Porridge is too hot";
            else
                return "Porridge is too cold";
        }
	}
	
	public static void sortGoldilocks(int p, int r) {
		if (r-p<1) return;
		int z = random.nextInt(r-p+1)+p;
		Goldilocks temp=G[r]; 
		Porridge temp2=null;
		G[r]=G[z];
		G[z]=temp;
		for (int i=p;i<=r;i++) {
			if (G[r].testPorridge(P[i]).equals("Yummm")) {
				temp2=P[r]; 
				P[r]=P[i];
				P[i]=temp2;
			}
		}
		Goldilocks gpivot=G[r];
		Porridge ppivot=P[r];
		int i=p-1;
		int j=p-1;
		while (true) {
			j++;
			if (gpivot.testPorridge(P[j]).equals("Porridge is too cold")) {
				i++;
				temp2=P[i];
				P[i]=P[j];
				P[j]=temp2;
			}
			if (j>=r-1) break;
		}
		temp2=P[i+1];
		P[i+1]=ppivot;
		P[r]=temp2;
		i=p-1;
		j=p-1;
		while (true) {
			j++;
			if (G[j].testPorridge(ppivot).equals("Porridge is too hot")) {
				i++;
				temp=G[i];
				G[i]=G[j];
				G[j]=temp;
			}
			if (j>=r-1) break;
		}
		temp=G[i+1];
		G[i+1]=gpivot;
		G[r]=temp;
		sortGoldilocks(p,i);
		sortGoldilocks(i+2,r);
	}
	
	public static void main(String[] args) {
		double[] nList = new double[100];
		double[] iterationList = new double[100];
		for (int i=0; i<10; i++) {
			int n=(int)Math.pow(2,i);
			nList[i]=n; 
			numIterations=0;
			Generator.setTemperatures(n);
			G = Generator.getGoldilocks();
			P = Generator.getPorridge();
			// System.out.println("before");
			// for (int k=0; k<n;k++) {
				// System.out.print(G[k].preference+",");
			// }
			// System.out.println();
			// for (int k=0; k<n;k++) {
				// System.out.print(P[k].getTemperature()+",");
			// }
			// System.out.println();
			sortGoldilocks(0,n-1);
			// System.out.println("after");
			// for (int k=0; k<n;k++) {
				// System.out.print(G[k].preference+",");
			// }
			// System.out.println();
			// for (int k=0; k<n;k++) {
				// System.out.print(P[k].getTemperature()+",");
			// }
			// System.out.println();
			iterationList[i]=numIterations;
			System.out.println("Size "+n+" data took "+numIterations+" to run");
			for (int j=0; j<n; j++) {	
				if (!G[j].testPorridge(P[j]).equals("Yummm")) {System.out.println("incorrect"); break;}
			}
		}
		Plot.plot(nList,iterationList,Color.GREEN,"Runtime analysis",true,false);
		
	}	
    
    
}