package hw2;

import java.util.Random;
import java.awt.Color;
import java.util.LinkedList;
import java.util.Collections;
import graph.*;

public class Generator {
	
	public static Random random = new Random();
	public static LinkedList<Integer> temperatures; 
	
	public static int[] getRandomArray(int n) {
		//int n = random.nextInt(n)+1;
		int[] res = new int[n];
		for (int i=0; i<n; i++) {
			res[i]=random.nextInt(65536)+1;
		}
		return res;
	}
	
	public static int getSum() {
		return (random.nextInt(65536)+1)*2;
	}
	
	public static void setTemperatures(int n) {
		temperatures = new LinkedList<Integer>();
		int current=0;
		for (int i=0; i<n; i++) {
			current+=random.nextInt(10)+1;
			temperatures.add(current);
		}
	}
	
	public static Goldilocks[] getGoldilocks() {
		Collections.shuffle(temperatures);
		Goldilocks[] res = new Goldilocks[temperatures.size()];
		for (int i=0; i<temperatures.size(); i++) {
			res[i] = new Goldilocks(temperatures.get(i));
		}
		return res;
	}
	
	public static Porridge[] getPorridge() {
		Collections.shuffle(temperatures);
		Porridge[] res = new Porridge[temperatures.size()];
		for (int i=0; i<temperatures.size(); i++) {
			res[i] = new Porridge(temperatures.get(i));
		}
		return res;
	}
	
	

}