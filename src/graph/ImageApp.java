package graph;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class ImageApp extends Component{

	private static final long serialVersionUID = 1L;
	
	public BufferedImage bi;
	int x;
	int y;
	int width;
	int height;
	boolean isNoBackground;
	boolean direction;

	
	public ImageApp(int x,int y, int width, int height) { //transparent blank image
		this.x=x; this.y=y;
		this.width=width;
		this.height=height;
		setBounds(x,y,width,height);
		bi=new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		Color transparent=new Color(0,0,0,0);
		for (int m=0; m<width; m++) {
			for (int n=0; n<height; n++) {
				bi.setRGB(m, n, transparent.getRGB());
			}
		}
	}
	
	public void colorBackground(Color c) {
		for (int m=0; m<width; m++) {
			for (int n=0; n<height; n++) {
				bi.setRGB(m, n, c.getRGB());
			}
		}
	}
	
	public void colorBorder(int numPixels,Color c) {
		for (int m=0; m<width; m++) {
			for (int n=0; n<height; n++) {
				if (m<numPixels||m>=width-numPixels||n<numPixels||n>=height-numPixels) {
					bi.setRGB(m, n, c.getRGB());
				}
			}
		}
	}
	
	public void noBackground() {
		Color transparent=new Color(0,0,0,0);
		for (int m=0; m<width; m++) {
			for (int n=0; n<height; n++) {
				Color target=new Color(bi.getRGB(m, n));
				if (target.getBlue()+target.getRed()+target.getGreen()>3*255-70) {
					bi.setRGB(m, n, transparent.getRGB());
				}
			}
		}
		isNoBackground=true;
	}
	
	public static void noBackground(BufferedImage bi) {
		Color transparent=new Color(0,0,0,0);
		for (int m=0; m<bi.getWidth(); m++) {
			for (int n=0; n<bi.getHeight(); n++) {
				Color target=new Color(bi.getRGB(m, n));
				if (target.getBlue()+target.getRed()+target.getGreen()>3*255-30) {
					bi.setRGB(m, n, transparent.getRGB());
				}
			}
		}
	}
	
	public void rescale(int x, int y) {
		width=x;
		height=y;
		BufferedImage old = bi;
		bi=new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		bi.getGraphics().drawImage(old, 0, 0,width,height, null);
		if (isNoBackground) {
			noBackground();
		}
	}
	
	public void flipHorizontal() {
		direction=!direction;
		BufferedImage result=new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for (int m=0; m<width; m++) {
			for (int n=0; n<height; n++) {
				int target=bi.getRGB(width-1-m, n);
				result.setRGB(m, n, target);
			}
		}
		bi=result;
	}
	
	public void transparent(int n) {
		for (int i=0; i<width;i++) {
			for (int j=0;j<height;j++) {
				Color c=new Color(bi.getRGB(i, j));
				Color c1=new Color(c.getRed(),c.getGreen(),c.getBlue(),n);
				bi.setRGB(i, j, c1.getRGB());
			}
		}
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}
	
	public void paint(Graphics g) {
		g.drawImage(bi,0,0,null);
	}
	
}
