package graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class PlotWindow {
	
	public static int graphSize = 500;
	public static int plotMargin = 100;
	public static int pointSize = 8;
	public JFrame frame;
	public JLayeredPane gp;
	public LinkedList<Double> xValues=new LinkedList<Double>();
	public LinkedList<Double> yValues=new LinkedList<Double>();
	public LinkedList<Color> colors=new LinkedList<Color>();
	public double xMin=Double.POSITIVE_INFINITY;
	public double xMax=Double.NEGATIVE_INFINITY;
	public double yMin=Double.POSITIVE_INFINITY;
	public double yMax=Double.NEGATIVE_INFINITY;
	
	public PlotWindow(String name) {
		Dimension d = new Dimension(graphSize,graphSize);
		frame=new JFrame(name);
		frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
		// frame.addComponentListener(new ComponentAdapter() {
			// public void componentResized(ComponentEvent e) {
				// if (gp!=null&&e.getID()==ComponentEvent.COMPONENT_RESIZED) {
					// graphSize = frame.getWidth();
					// Dimension d = new Dimension(graphSize,graphSize);
					// gp.setSize(d);
					// gp.setMinimumSize(d);
					// paint();
				// }
			// }
		// });
		frame.setSize(d);
		frame.setMinimumSize(d);
		gp=new JLayeredPane();
		gp.setVisible(false);
		gp.setSize(d);
		gp.setMinimumSize(d);
		frame.add(gp);
		gp.setVisible(true);
		frame.setVisible(true);
	}
	
	public void plotAxis() {
		ImageApp im = new ImageApp(0, 0, graphSize, graphSize);
		for (int i=plotMargin;i<graphSize-plotMargin;i++) {
			im.bi.setRGB(i,graphSize-plotMargin,Color.BLACK.getRGB());
			im.bi.setRGB(plotMargin,i,Color.BLACK.getRGB());
		}
		gp.add(im,new Integer(0));
		JLabel l1 = new JLabel(""+xMin);
		l1.setBounds(plotMargin,graphSize-plotMargin+5,75,10);
		gp.add(l1,new Integer(1));
		JLabel l2 = new JLabel(""+xMax);
		l2.setBounds(graphSize-plotMargin-30,graphSize-plotMargin+5,75,10);
		gp.add(l2,new Integer(1));
		JLabel l3 = new JLabel(""+yMax);
		l3.setBounds(plotMargin-30,plotMargin-15,75,10);
		gp.add(l3,new Integer(1));
		JLabel l4 = new JLabel(""+yMin);
		l4.setBounds(plotMargin-30,graphSize-plotMargin-15,75,10);
		gp.add(l4,new Integer(1));
	}
	
	public void addPoints(double[] x, double[] y, Color c) {
		if (x.length!=y.length) {
			System.out.println("Array length mismatch"); return;
		}
		int n = x.length;
		for (int i=0; i<n; i++) {
			if (x[i]<xMin) xMin=x[i];
			if (x[i]>xMax) xMax = x[i];
			if (y[i]<yMin) yMin=y[i];
			if (y[i]>yMax) yMax = y[i];
			xValues.add(x[i]); yValues.add(y[i]); colors.add(c);
		}
		
	}
	
	public synchronized void paint(boolean hasLabel) {
		if (xValues.size()==0) return;
		gp.removeAll();
		plotAxis();
		int plotSize = graphSize-2*plotMargin;
		for (int i=0; i<xValues.size(); i++) {
			int pointX = (int)((xValues.get(i)-xMin)/(xMax-xMin)*plotSize-pointSize/2+plotMargin);
			int pointY = (int)((yValues.get(i)-yMax)/(yMin-yMax)*plotSize-pointSize/2+plotMargin);
			ImageApp im = new ImageApp(pointX, pointY, pointSize, pointSize);
			im.colorBackground(colors.get(i));
			gp.add(im,new Integer(2));
			if (hasLabel) {
				JLabel label = new JLabel("("+xValues.get(i)+","+yValues.get(i)+")");
				label.setBounds(pointX,pointY-20,100,10);
				gp.add(label,new Integer(3));
			}			
		}
		gp.repaint();
		frame.repaint();
	}

}
