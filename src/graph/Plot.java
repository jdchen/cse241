package graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import java.util.LinkedList;

public class Plot {
	
	public static int graphSize = 300;
	public static PlotWindow p;
	public static LinkedList<PlotWindow> windows=new LinkedList<PlotWindow>();
	
	public static void plot(double[] xValues,double[] yValues,Color c,String label,boolean isNewWindow, boolean hasLabel) {
		if (p==null||isNewWindow) {
			p = new PlotWindow(label);
			windows.add(p);
		}
		p.addPoints(xValues, yValues,c);
		p.paint(hasLabel);
	}
	
	public static void plot(LinkedList<Double> xValues,LinkedList<Double> yValues,Color c,String label,boolean isNewWindow, boolean hasLabel) {
		if (p==null||isNewWindow) {
			p = new PlotWindow(label);
			windows.add(p);
		}
		double[] x = new double[xValues.size()];
		double[] y = new double[yValues.size()];
		for (int i=0; i<xValues.size();i++) {
			x[i]=xValues.get(i);
			y[i]=yValues.get(i);
		}
		p.addPoints(x, y,c);
		p.paint(hasLabel);
	}
	
	public static void main(String[] args) {
		double[] x = {0,1,2,3};
		double[] y = {0,1,2,3};
		plot(x,y,Color.BLACK,"test",true,true);
	}

}
