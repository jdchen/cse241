package hw4;

import java.util.Random;
import java.util.Collections;
import java.util.ArrayList;

public class Generator {
	
	public static Random random = new Random();
	
	public static ColorNum[] getColorNum(int n) {
		ColorNum[] res = new ColorNum[n];
		String s;
		int b,c=0;
		for (int i=0; i<n; i++) {
			b = random.nextInt(2);
			if (b==0) s="blue"; else s="white";
			//res[i]=new ColorNum(random.nextInt(65536)+1,s);
			c+=random.nextInt(20)+1;
			res[i]=new ColorNum(c,s);
		}
		ArrayList<ColorNum> list = new ArrayList<ColorNum>();
		for (ColorNum cn:res) list.add(cn);
		Collections.shuffle(list);
		return list.toArray(new ColorNum[0]);
	}
	

}