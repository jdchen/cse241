package hw4;

public class ColorNum {
    
    public int x;
	public String color;
	public ColorNum closest;
	public ColorNum left;
	public ColorNum right;
    public ColorNum parent;
	
    public ColorNum(int x,String color) {
        this.x = x;
		this.color = color;
    }
	
	public ColorNum predecessor() {
		ColorNum c = this;
		if (c.left!=null) return c.left.max();
		while (c.parent!=null&&c==c.parent.left) {
			c=c.parent;
		}
		if (c==this) return null;
		else return c;
	}
	
	public ColorNum successor() {
		ColorNum c = this;
		if (c.right!=null) return c.right.min();
		while (c.parent!=null&&c==c.parent.right) {
			c=c.parent;
		}
		if (c==this) return null;
		else return c;
	}
	
	public ColorNum max() {
		ColorNum c = this;
		while(c.right!=null) c=c.right;
		return c;
	}
	
	public ColorNum min() {
		ColorNum c = this;
		while(c.left!=null) c=c.left;
		return c;
	}
    
    
}