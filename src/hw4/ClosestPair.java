package hw4;

import graph.*;

public class ClosestPair {
    
	public static ColorNum root;
    public static ColorNum p;
    public static ColorNum q;
	public static ColorNum[] A;
	public static int min=Integer.MAX_VALUE;
        
    public static boolean isCorrect() {
		ColorNum one=null,two=null;
		int tmp=Integer.MAX_VALUE;
		for (int i=0; i<A.length-1;i++) {
			for (int j=i+1;j<A.length;j++) {
				int diff = A[i].x-A[j].x;
				if (diff<0) diff=-diff;
				if (!A[i].color.equals(A[j].color)&&diff<tmp) {
					tmp=diff;
					one=A[i]; two=A[j];
				}
			}
		}
		if (one==p&&two==q&&tmp==min) return true;
		else return false;
    }
	
	public static void closest(){
		root=null;
		for (ColorNum cn: A) insert(cn);
	}
	
	public static void insert(ColorNum cn) {
		System.out.println("inserting: "+cn.x+" "+cn.color);
		if (root==null) root=cn;
		else {
			ColorNum current = root;
			int target = cn.x;
			while (true) {
				if (current.x<target&&current.right!=null) current=current.right;
				else if (current.x>target&&current.left!=null) current=current.left;
				else if (current.x==target) {System.out.println("found duplicate"); return;}
				else break;
			}
			if (current.x<target) {current.right=cn; cn.parent=current;}
			else if (current.x>target) {current.left=cn; cn.parent=current;}
			ColorNum l = cn.predecessor();
			ColorNum r = cn.successor();
		}
	}
    
	public static double[] toDouble(long[] x){
		double[] result = new double[x.length];
		for (int i=0; i<x.length; i++) {
			result[i]=1.0*x[i];
		}
		return result;
	}
	
    public static void main(String[] args) {
		double[] nList = new double[100];
		long[] durationList = new long[100];
		for (int i=1; i<3; i++) {
			//int n=(int)Math.pow(10,i);
			int n=i*2;
			nList[i]=n; 
			A = Generator.getColorNum(n);
			//int[] a = {4,1,3,4,3,3,1,4};
			long startTime = System.currentTimeMillis();
			closest();
			long endTime = System.currentTimeMillis();
			durationList[i]=endTime-startTime;
			System.out.println("Size "+n+" data took "+durationList[i]+" to run");
			if (!isCorrect()) {
				System.out.println("incorrect");
			}
		}
		//Plot.plot(nList,toDouble(durationList),Color.GREEN,"Runtime analysis",true,false);
    }
}