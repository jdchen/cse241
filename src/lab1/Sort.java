package lab1;
import java.util.LinkedList;
import java.util.Random;

public class Sort {
    
    //================================= sort =================================
    //
    // Input: array A of XYPoints refs 
    //        lessThan is the function used to compare points
    //
    // Output: Upon completion, array A is sorted in nondecreasing order
    //         by lessThan.
    //
    // DO NOT USE ARRAYS.SORT.  WE WILL CHECK FOR THIS.  YOU WILL GET A 0.
    // I HAVE GREP AND I AM NOT AFRAID TO USE IT.
    //=========================================================================
    public static Comparator comp;
	public static XYPoint[] B;
	public static Random random = new Random();
	public static XYPoint temp;
	public static int threshold=10;
    
    public static void msort(XYPoint[] A, Comparator lessThan) {
	   //Implement your sort here.  (use mergeSort).
		comp=lessThan;
		B=A;
		quicksort(0,B.length-1);
		//insertionSort(0,B.length-1);
    }
    
    public static LinkedList<XYPoint> merge(LinkedList<XYPoint> A) {
	int n=A.size();
	if (n<=1) return A;
	LinkedList<XYPoint> left = new LinkedList<XYPoint>();
	LinkedList<XYPoint> right = new LinkedList<XYPoint>();
	int j=0;
	for (XYPoint xy: A) {
	    if (j<n/2) left.addLast(xy);
	    else right.addLast(xy);
	    j++;
	}
	left=merge(left);
	right=merge(right);
	LinkedList<XYPoint> combined =new LinkedList<XYPoint>();
	for (int i=0; i<n; i++) {
	    if (left.isEmpty()&&!right.isEmpty()) {
		combined.add(right.removeFirst());
	    } else if (!left.isEmpty()&&right.isEmpty()) {
		combined.add(left.removeFirst());
	    }else if (comp.comp(left.getFirst(),right.getFirst())) {
		combined.add(left.removeFirst());
	    } else {
		combined.add(right.removeFirst());
	    }
	}
	return combined;
    }
	
	public static void quicksort(int p, int r) {
		if (r-p<1) return;
		int z = random.nextInt(r-p+1)+p;
		temp=B[r];
		B[r]=B[z];
		B[z]=temp;
		if (r-p<threshold) {
			insertionSort(p,r); //faster for small n
		} else {
			int i=p-1;
			int j=p-1;
			while (true) {
				j++;
				if (comp.comp(B[j],B[r])) {
					i++;
					temp=B[i];
					B[i]=B[j];
					B[j]=temp;
				}
				if (j>=r-1) break;
			}
			temp=B[i+1];
			B[i+1]=B[r];
			B[r]=temp;
			quicksort(p,i);
			quicksort(i+2,r);
		}
	}
	
	public static void insertionSort(int p, int r) {
		int n=r-p+1;
		if (n<2) return;
		for (int j=p+1;j<=r;j++) {
			XYPoint current = B[j];
			int i=j-1;
			while (i>=p&&comp.comp(current,B[i])) {
				B[i+1]=B[i];
				i--;
			}
			B[i+1]=current;
		}
		
	}
    
}
