package lab1;
import java.util.LinkedList;

public class ClosestPair {

   public final static double INF = java.lang.Double.POSITIVE_INFINITY;
	public static int threshold=15;
	public static XYPoint[] pointsByX;
	public static XYPoint[] pointsByY;
    /** 
     * Given a collection of points, find the closest pair of point and the
     * distance between them in the form "(x1, y1) (x2, y2) distance"
     *
     * @param pointsByX points sorted in nondecreasing order by X coordinate
     * @param pointsByY points sorted in nondecreasing order by Y coordinate
     * @return Result object containing the points and distance
     */
	 
	 public static Result findClosestPair(XYPoint pbX[], XYPoint pbY[]) {
		pointsByX=pbX;
		pointsByY=pbY;
		int[] ya = new int[pointsByY.length];
		for (int i=0; i<pointsByY.length; i++) {
			ya[i]=i;
		}
		return closestPair(0,pointsByX.length-1,ya);
	 }
	 
   public static Result closestPair(int xp,int xr, int[] ya) {
      int n = xr-xp+1;
      if (n<2) return new Result(pointsByX[xp],pointsByX[xp], INF);
      if (n==2) return new Result(pointsByX[xp],pointsByX[xr],pointsByX[xp].dist(pointsByX[xr]));
	  if (n<threshold) { //using brute force for small data sets is more efficient
		int p1=xp,p2=xp+1;
		double temp,mindist=INF;
		for (int i=xp; i<=xr; i++) {
			for (int j=i+1; j<=xr; j++) {
				temp=pointsByX[i].dist(pointsByX[j]);
				if (temp<mindist) {
					p1=i; 
					p2=j;
					mindist=temp;
				}
			}
		}
		return new Result(pointsByX[p1],pointsByX[p2],mindist);
	  }
      int mid = xp+n/2;
      int[] yl = new int[n/2];
      int[] yr = new int[n-n/2];
	  int j1=0,j2=0;
      for (int i=0; i<n; i++) {
         if (pointsByY[ya[i]].isLeftOf(pointsByX[mid])) {
			yl[j1]=ya[i]; j1++;
         }else {
			yr[j2]=ya[i]; j2++;
		}
	  }
      Result lmin = closestPair(xp,mid-1,yl);
      Result rmin = closestPair(mid,xr,yr);
      Result lrmin;
      if (lmin.dist<rmin.dist) lrmin=lmin;
      else lrmin=rmin;
      int[] ystrip = new int[n];
	  int ystriplength=0;
      for (int xy:ya){
         if (abs(pointsByY[xy].x-pointsByX[mid].x)<lrmin.dist) {
			ystrip[ystriplength]=xy; ystriplength++;
		}
      }
      for (int i=0; i<ystriplength; i++) {
		int l=0;
         for (int k=i+1; k<ystriplength&&l<8; k++,l++) {
            if (abs(pointsByY[ystrip[k]].y-pointsByY[ystrip[i]].y)<lrmin.dist) {
               double tempdist=pointsByY[ystrip[k]].dist(pointsByY[ystrip[i]]);
               if (tempdist<lrmin.dist) {
					lrmin.p1=pointsByY[ystrip[i]];
					lrmin.p2=pointsByY[ystrip[k]];
					lrmin.dist=tempdist;
				}
            }
         }
      }
      return lrmin;
   }
   
   public static Result bruteForce(XYPoint pointsByX[], XYPoint pointsByY[]) {
		int n = pointsByX.length;
		int p1=0,p2=1;
		double temp,mindist=INF;
		for (int i=0; i<n; i++) {
			for (int j=i+1; j<n; j++) {
				temp=pointsByX[i].dist(pointsByX[j]);
				if (temp<mindist) {
					p1=i; 
					p2=j;
					mindist=temp;
				}
			}
		}
		return new Result(pointsByX[p1],pointsByX[p2],mindist);
   }
   
    
    static int abs(int x) {
         if (x < 0) {
             return -x;
         } else {
             return x;
         }
   }
}
