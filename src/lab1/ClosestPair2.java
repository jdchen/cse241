package lab1;

import java.util.LinkedList;

public class ClosestPair2 {

   public final static double INF = java.lang.Double.POSITIVE_INFINITY;

    /** 
     * Given a collection of points, find the closest pair of point and the
     * distance between them in the form "(x1, y1) (x2, y2) distance"
     *
     * @param pointsByX points sorted in nondecreasing order by X coordinate
     * @param pointsByY points sorted in nondecreasing order by Y coordinate
     * @return Result object containing the points and distance
     */
   static Result findClosestPair(XYPoint pointsByX[], XYPoint pointsByY[]) {
      int n = pointsByX.length;
      if (n<2) return new Result(pointsByX[0],pointsByX[0], INF);
      if (n==2) return new Result(pointsByX[0],pointsByX[1],pointsByX[0].dist(pointsByX[1]));
      XYPoint[] xl = new XYPoint[n/2];
      XYPoint[] xr = new XYPoint[n-n/2];
      LinkedList<XYPoint> yl = new LinkedList<XYPoint>();
      LinkedList<XYPoint> yr = new LinkedList<XYPoint>();
      for (int i=0; i<n; i++) {
         if (i<n/2) xl[i]=pointsByX[i];
         else xr[i-n/2]=pointsByX[i];
         if (pointsByY[i].isLeftOf(pointsByX[n/2])) yl.add(pointsByY[i]);
         else yr.add(pointsByY[i]);
      }
      Result lmin = findClosestPair(xl,yl.toArray(new XYPoint[0]));
      Result rmin = findClosestPair(xr,yr.toArray(new XYPoint[0]));
      Result lrmin;
      if (lmin.dist<rmin.dist) lrmin=lmin;
      else lrmin=rmin;
      LinkedList<XYPoint> ystriplist = new LinkedList<XYPoint>();
      for (XYPoint xy:pointsByY){
         if (abs(xy.x-xr[0].x)<lrmin.dist) ystriplist.add(xy);
      }
      XYPoint[] ystrip = ystriplist.toArray(new XYPoint[0]);
      for (int i=0; i<ystrip.length; i++) {
         for (int k=i+1; k<ystrip.length; k++) {
            if (abs(ystrip[k].y-ystrip[i].y)<lrmin.dist) {
               double tempdist=ystrip[k].dist(ystrip[i]);
               if (tempdist<lrmin.dist)
                  lrmin=new Result(ystrip[i],ystrip[k],tempdist);
            }
         }
      }
      return lrmin;
   }
   
    
    static int abs(int x) {
         if (x < 0) {
             return -x;
         } else {
             return x;
         }
   }
}
