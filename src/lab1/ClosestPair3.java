package lab1;
import java.util.LinkedList;

public class ClosestPair3 {

   public final static double INF = java.lang.Double.POSITIVE_INFINITY;
	public static int threshold=15;
	public static XYPoint[] pointsByX;
    /** 
     * Given a collection of points, find the closest pair of point and the
     * distance between them in the form "(x1, y1) (x2, y2) distance"
     *
     * @param pointsByX points sorted in nondecreasing order by X coordinate
     * @param pointsByY points sorted in nondecreasing order by Y coordinate
     * @return Result object containing the points and distance
     */
	 
	 public static Result findClosestPair(XYPoint pbX[], XYPoint pointsByY[]) {
		pointsByX=pbX;
		return closestPair(0,pointsByX.length-1,pointsByY);
	 }
	 
   public static Result closestPair(int xp,int xr, XYPoint pointsByY[]) {
      int n = xr-xp+1;
      if (n<2) return new Result(pointsByX[xp],pointsByX[xp], INF);
      if (n==2) return new Result(pointsByX[xp],pointsByX[xr],pointsByX[xp].dist(pointsByX[xr]));
	  if (n<threshold) { //using brute force for small data sets is more efficient
		int p1=xp,p2=xp+1;
		double temp,mindist=INF;
		for (int i=xp; i<=xr; i++) {
			for (int j=i+1; j<=xr; j++) {
				temp=pointsByX[i].dist(pointsByX[j]);
				if (temp<mindist) {
					p1=i; 
					p2=j;
					mindist=temp;
				}
			}
		}
		return new Result(pointsByX[p1],pointsByX[p2],mindist);
	  }
      int mid = xp+n/2;
      XYPoint[] yl = new XYPoint[n/2];
      XYPoint[] yr = new XYPoint[n-n/2];
	  int j1=0,j2=0;
      for (int i=0; i<n; i++) {
         if (pointsByY[i].isLeftOf(pointsByX[mid])) {
			yl[j1]=pointsByY[i]; j1++;
         }else {
			yr[j2]=pointsByY[i]; j2++;
		}
	  }
      Result lmin = closestPair(xp,mid-1,yl);
      Result rmin = closestPair(mid,xr,yr);
      Result lrmin;
      if (lmin.dist<rmin.dist) lrmin=lmin;
      else lrmin=rmin;
      LinkedList<XYPoint> ystriplist = new LinkedList<XYPoint>();
      for (XYPoint xy:pointsByY){
         if (abs(xy.x-pointsByX[mid].x)<lrmin.dist) ystriplist.add(xy);
      }
      XYPoint[] ystrip = ystriplist.toArray(new XYPoint[0]);
      for (int i=0; i<ystrip.length; i++) {
		int l=0;
         for (int k=i+1; k<ystrip.length&&l<8; k++,l++) {
            if (abs(ystrip[k].y-ystrip[i].y)<lrmin.dist) {
               double tempdist=ystrip[k].dist(ystrip[i]);
               if (tempdist<lrmin.dist) {
					lrmin.p1=ystrip[i];
					lrmin.p2=ystrip[k];
					lrmin.dist=tempdist;
				}
            }
         }
      }
      return lrmin;
   }
   
   public static Result bruteForce(XYPoint pointsByX[], XYPoint pointsByY[]) {
		int n = pointsByX.length;
		int p1=0,p2=1;
		double temp,mindist=INF;
		for (int i=0; i<n; i++) {
			for (int j=i+1; j<n; j++) {
				temp=pointsByX[i].dist(pointsByX[j]);
				if (temp<mindist) {
					p1=i; 
					p2=j;
					mindist=temp;
				}
			}
		}
		return new Result(pointsByX[p1],pointsByX[p2],mindist);
   }
   
    
    static int abs(int x) {
         if (x < 0) {
             return -x;
         } else {
             return x;
         }
   }
}
