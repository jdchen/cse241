package lab2;

import java.util.*;

/**
 * Record type for string hash table
 *
 * A record associates a certain string (the key value) with
 * a list of sequence positions at which that string occurs
 *
 * Change anything you want about this except the methods we have already.
 */
public class Record {
    private final String key;
	private int intKey;
    private final ArrayList<Integer> positions;

    /**
     * Constructs a Record with the given string
     *
     * @param s key of the Record
     */
    public Record(String s) {
        key = s;
		intKey = 0;
        positions = new ArrayList<Integer>(1);
    }
	
	public void setIntKey(int d) {
        intKey=d;
    }
	
	public int getIntKey() {
        return intKey;
    }

    /**
     * Returns the key associated with this Record.
     */
    public String getKey() {
        return key;
    }

    /**
     * Adds a new position to the positions list.
     *
     * @param position of the key
     */
    public void addPosition(Integer position) {
        positions.add(position);
    }

    /**
     * Returns how many positions we have
     */
    public int getPositionsCount() {
        return positions.size();
    }

    /**
     * Positions accessor
     */
    public Integer getPosition(int index) {
        return positions.get(index);
    }
}